//path
const path = require('path')
var pathObj = path.parse(__filename)
console.log(pathObj);

const fs = require('fs');
fs.readFile(__dirname +'./filename.txt', 'utf-8', (err,content) =>{
});

// const fs = require('fs');
// const files = fs.readdirSync('./');
// console.log(files);

// changed './' to $ in order to get error

fs.readdir('./', function (err, files) {
    if (err) console.log('Error', err);
    else console.log('result', files);
});

// Infromation about operationg system

const os = require('os');
console.log('SYSTEM MEMORY', (os.totalmem() / 1024 / 1024 / 1024).toFixed(2) + 'GB');
console.log('FREE MEMORY', (os.freemem() / 1024 / 1024 / 1024).toFixed(2) + 'GB');
console.log("ARCH: " + os.arch());
console.log("CPU CORE: " + os.cpus());
console.log("RELEASE: " + os.release());
console.log("USER INO: " + os.userInfo());
console.log("PLATFORM: " + os.platform());

// reading a file content

const readline = require('readline');
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

const message =
    `choose an option:
1. Read my package.json
2. Get my OS info
3. Start a http Server
-------------------------
Type a number: 1`

rl.question('this >', (answer) => {

    if (answer == 2) {
        console.log('nice work! That is correct');
    } else if (answer == 11) {
        console.log('Are you a JS programmer');

    } else {
        console.log('Thats not right.');

    }
});

//HTTP REQUEST MODULE
//----------------------------------------

const EventEmitter = require('events');
//Register a listener
const Logger = require('./logger');
const logger = new Logger();

logger.on('messageLogged', (arg) => {
    console.log('Listener called', arg);
});
logger.log('message');



const http = require ('http');
const server = http.createServer((req, res) => {
    if (req.url ==='/') {
      res.write('Hello World From Tina');
      res.end();  
    }
if (req.url ==='/api/courses') {
    res.write(JSON.stringify( [1, 2, 3] ));
    res.end();
}
});


server.listen(3000);

console.log('listening on port 3000...');
